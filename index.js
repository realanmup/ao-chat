var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var port = process.env.PORT || 3000;

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/index.html");
});

io.on("connection", function(socket) {
  socket.on("anmup", function(data) {
    io.emit("anmup", data);
  });
  socket.on("anmup?", function(data) {
    io.emit(data, "anmup?");
  });
  /**
   * Join RoomId
   * @param  {[type]} roomId){               	socket.join(roomId);  } [description]
   * @return {[type]}           [description]
   */
  socket.on("join", function(roomId) {
    socket.join(roomId);
  });

  /**
   * Leave room
   */
  socket.on("leave", function(roomId) {
    socket.leave(roomId);
  });

  /**
   * private Message
   * data => { receiver_id, sender_id, message }
   * @return {[type]} [description]
   */
  socket.on("message", function(data) {
    if (!data.roomId) return console.log("error here");

    io.to(data.roomId).emit(data.roomId, data);
  });
});

http.listen(port, function() {
  console.log("listening on *:" + port);
});
